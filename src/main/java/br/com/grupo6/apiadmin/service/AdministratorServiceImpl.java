package br.com.grupo6.apiadmin.service;

import br.com.grupo6.apiadmin.dao.AdministratorDAO;
import br.com.grupo6.apiadmin.dto.AdministratorInfosGetDTO;
import br.com.grupo6.apiadmin.dto.AdministratorLoginDTO;
import br.com.grupo6.apiadmin.exception.ConstraintException;
import br.com.grupo6.apiadmin.model.Administrator;
import br.com.grupo6.apiadmin.security.AdministratorCrypto;
import br.com.grupo6.apiadmin.security.Token;
import br.com.grupo6.apiadmin.security.TokenUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@Log4j2
public class AdministratorServiceImpl implements IAdministratorService {

    @Autowired
    private AdministratorDAO dao;

//    @Autowired
//    private PasswordEncoder encoder;

    @Override
    public Token generateLoggedAdministratorToken(AdministratorLoginDTO loginData) throws Exception{
        Administrator user = dao.findByUsernameOrEmail(loginData.getUsername(), loginData.getEmail());
        try {

            if (user != null) {
                String passwordLogin = AdministratorCrypto.encrypt(loginData.getPassword());
//                String passwordLogin = (loginData.getPassword());

//                boolean passwordsMatch = encoder.matches(passwordLogin, user.getPassword());
//                log.info(passwordsMatch);
//                if (passwordsMatch){
////                    return user;
//                    return new Token(TokenUtil.createToken(user));
//                }

//                log.info((passwordLogin.equals(user.getPassword()));
                if (passwordLogin.equals(user.getPassword())) {
                    return new Token(TokenUtil.createToken(user));
                }
            }
        } catch(Exception e) {
            throw new Exception("Unknown error: " + e.getMessage());
        }

        return null;
    }

    @Override
    public Administrator createAdministrator(Administrator novo) throws ConstraintException, Exception {
        try {
            if (novo.getPassword() != null) {
                novo.setPassword(AdministratorCrypto.encrypt(novo.getPassword()));
//                novo.setPassword(encoder.encode(novo.getPassword()));
                dao.save(novo);
                return novo;
            }
        } catch(DataIntegrityViolationException e) {
            throw new ConstraintException("Constraint Problem:" + e.getMostSpecificCause().getMessage());
        }
        catch(Exception e) {
            throw new Exception("Unknown error: " + e.getMessage());
        }
        return null;
    }


    @Override
    public Administrator changeData(Administrator data) throws Exception{

        try {
            if (data.getPassword() != null) {
//                data.setPassword(encoder.encode(data.getPassword()));
                data.setPassword(AdministratorCrypto.encrypt(data.getPassword()));
                dao.save(data);
                return data;
            }

        } catch(Exception e) {
            throw new Exception("Unknown error: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<AdministratorInfosGetDTO> getAllAdministrators() {
        return dao.getAllAdministrators();
    }

    @Override
    public AdministratorInfosGetDTO getAdministratorById(UUID id) throws Exception, ConstraintException {
        AdministratorInfosGetDTO administrator = dao.getAdministratorById(id);

        try {
            return administrator;
        } catch(DataIntegrityViolationException e) {
            throw new ConstraintException("Constraint Problem:" + e.getMostSpecificCause().getMessage());
        }
        catch(Exception e) {
            throw new Exception("Unknown error: " + e.getMessage());
        }
    }

}