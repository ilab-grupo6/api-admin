package br.com.grupo6.apiadmin.service;

import br.com.grupo6.apiadmin.dto.AdministratorInfosGetDTO;
import br.com.grupo6.apiadmin.dto.AdministratorLoginDTO;
import br.com.grupo6.apiadmin.model.Administrator;
import br.com.grupo6.apiadmin.security.Token;

import java.util.List;
import java.util.UUID;

public interface IAdministratorService {
    public Token generateLoggedAdministratorToken(AdministratorLoginDTO loginData) throws Exception;
    public Administrator createAdministrator(Administrator novo) throws Exception;

    Administrator changeData(Administrator data) throws Exception;

    public List<AdministratorInfosGetDTO> getAllAdministrators();

    public AdministratorInfosGetDTO getAdministratorById(UUID id) throws Exception;
}
