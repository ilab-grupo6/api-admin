package br.com.grupo6.apiadmin.security;

import br.com.grupo6.apiadmin.model.Administrator;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Collections;
import java.util.Date;

public class TokenUtil {

    private static final String HEADER = "Authorization";
    private static final String PREFIX = "Bearer ";
    private static final long   EXPIRATION = 20*1000*60;
    private static final String SECRET_KEY = "3c0MMerc3Do1f00dP@r@T3st3sD3JWT*";
    private static final String ISSUER = "Grupo6";


    public static String createToken(Administrator administrator) {

        Key secretKey = Keys.hmacShaKeyFor(SECRET_KEY.getBytes());

        String token = Jwts.builder().setSubject(administrator.getUsername())
                .setIssuer(ISSUER)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION))
                .signWith(secretKey, SignatureAlgorithm.HS256)
                .compact();
        System.out.println("TOKEN gerado = "+token);
        return PREFIX + token;
    }

    private static boolean isExpirationValid(Date expiration) {
        return expiration.after(new Date(System.currentTimeMillis()));
    }
    private static boolean isIssuerValid(String issuer) {
        return issuer.equals(ISSUER);
    }
    private static boolean isSubjectValid(String username) {
        return username!=null && username.length() > 0;
    }

    public static Authentication validate(HttpServletRequest request) {
        String token = request.getHeader(HEADER);
        token = token.replace(PREFIX, "");

        Jws<Claims> jwsClaims = Jwts.parserBuilder().setSigningKey(SECRET_KEY.getBytes())
                .build()
                .parseClaimsJws(token);

        String username = jwsClaims.getBody().getSubject();
        String issuer   = jwsClaims.getBody().getIssuer();
        Date expire   = jwsClaims.getBody().getExpiration();

        if (isSubjectValid(username) && isIssuerValid(issuer) && isExpirationValid(expire)) {
            return new UsernamePasswordAuthenticationToken(username, null, Collections.emptyList());
        }

        return null;
    }

}
