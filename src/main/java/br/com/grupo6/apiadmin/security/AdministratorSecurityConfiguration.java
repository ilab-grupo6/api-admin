package br.com.grupo6.apiadmin.security;

import br.com.grupo6.apiadmin.service.IAdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class AdministratorSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AdministratorEntryPoint entryPoint;

//    @Bean
//    public PasswordEncoder getPasswordEncoder() {
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        return encoder;
//    }

//    private final PasswordEncoder passwordEncoder;
//
//    private final IAdministratorService administratorService;
//
//    public AdministratorSecurityConfiguration(PasswordEncoder passwordEncoder, IAdministratorService administratorService) {
//        this.passwordEncoder = passwordEncoder;
//        this.administratorService = administratorService;
//    }

//    @Override
//    protected  void configure (AuthenticationManagerBuilder auth) throws  Exception{
//        auth.userDetailsService(administratorService).passwordEncoder(passwordEncoder);
//    }

    @Override
    protected void configure(HttpSecurity httpSec) throws Exception{
        System.out.println("--> SETUP da configuração de segurança...");

        httpSec.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(entryPoint)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/admin/signup").permitAll()
                .antMatchers("/swagger-ui*/**", "/techgeeknext-openapi/**", "/api-docs*/**").permitAll()
                .anyRequest().authenticated().and().cors();

        httpSec.addFilterBefore(new AdministratorFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}
