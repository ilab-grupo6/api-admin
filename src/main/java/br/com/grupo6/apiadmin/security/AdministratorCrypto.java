package br.com.grupo6.apiadmin.security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class AdministratorCrypto {

    public static String encrypt(String original) throws Exception {

//        String strMyKey = "123456789 123456789 123456789 12";
        String strMyKey = "GRUPO SEIS E O MELHOR GRUPO 06 6";
        Key myKey = new SecretKeySpec(strMyKey.getBytes(), "AES");

        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.ENCRYPT_MODE, myKey);

        cipher.update(original.getBytes());

        String originalCripto = new String(cipher.doFinal(), "UTF-8");
        byte[] test = originalCripto.getBytes();

        StringBuilder cryptoHex = new StringBuilder();
        for (byte b: test) {
            cryptoHex.append(Integer.toHexString(b));
        }

        return cryptoHex.toString();
    }

}
