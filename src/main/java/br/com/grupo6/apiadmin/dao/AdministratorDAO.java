package br.com.grupo6.apiadmin.dao;

import br.com.grupo6.apiadmin.dto.AdministratorInfosGetDTO;
import br.com.grupo6.apiadmin.model.Administrator;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface AdministratorDAO extends CrudRepository<Administrator, UUID> {
    public Administrator findByUsernameOrEmail(String username, String email);

    @Query(
            "    SELECT "
                    + "new br.com.grupo6.apiadmin.dto.AdministratorInfosGetDTO("
                    + "  administrator.id,"
                    + "  administrator.username,"
                    + "  administrator.email,"
                    + "  administrator.name)"
                    + "FROM Administrator as administrator"
    )
    public List<AdministratorInfosGetDTO> getAllAdministrators();


    @Query(
            "    SELECT "
                    + "new br.com.grupo6.apiadmin.dto.AdministratorInfosGetDTO("
                    + "  administrator.id,"
                    + "  administrator.username,"
                    + "  administrator.email,"
                    + "  administrator.name)"
                    + "FROM Administrator as administrator WHERE administrator.id = :id")
    public AdministratorInfosGetDTO getAdministratorById(@Param("id") UUID id);
}
