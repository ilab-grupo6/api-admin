package br.com.grupo6.apiadmin.model;

import br.com.grupo6.apiadmin.validation.PostValidation;
import br.com.grupo6.apiadmin.validation.PutValidation;
//import jakarta.persistence.*;
//import jakarta.persistence.Column;
//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Entity
@Table(name="admin_teste")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Administrator {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name="id")
//    private UUID id;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @Column(name="username", length = 20, nullable = false, unique = true)
    @NotBlank(message = "username is required", groups = {PostValidation.class, PutValidation.class})
    private String  username;

    @Column(name="password", columnDefinition = "TEXT", nullable = false)
    @NotBlank(message = "password is required", groups = PostValidation.class)
    private String  password;

    @Column(name="email", length = 100, nullable = false, unique = true)
    @NotBlank(message = "Email is required", groups = PostValidation.class)
    @Email(message = "Email should be valid", groups = {PostValidation.class, PutValidation.class})
    private String  email;

    @Column(name="name", length = 100, nullable = false)
    @NotBlank(message = "Name is required", groups = PostValidation.class)
    private String name;

}
