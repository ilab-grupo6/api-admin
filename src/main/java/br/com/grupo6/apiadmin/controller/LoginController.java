package br.com.grupo6.apiadmin.controller;

import br.com.grupo6.apiadmin.dto.AdministratorLoginDTO;
import br.com.grupo6.apiadmin.security.Token;
import br.com.grupo6.apiadmin.service.IAdministratorService;
import br.com.grupo6.apiadmin.validation.PostValidation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class LoginController {

    @Autowired
    private IAdministratorService service;

    @PostMapping("/login")
    @Operation(summary = "Logar pessoa administradora", description = "Este endpoint faz a autenticação da pessoa administradora já cadastrada na base de dados, e retorna um token de autenticação para acesso aos endpoints protegidos.")
    public ResponseEntity<Token> signIn(@Validated(PostValidation.class) @RequestBody AdministratorLoginDTO loginData ) throws Exception {
        Token token = service.generateLoggedAdministratorToken(loginData);
        if (token != null) {
            return ResponseEntity.ok(token);
        }
        return ResponseEntity.status(401).build();
    }


}