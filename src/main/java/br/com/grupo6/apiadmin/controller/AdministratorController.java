package br.com.grupo6.apiadmin.controller;

import br.com.grupo6.apiadmin.dto.AdministratorInfosGetDTO;
import br.com.grupo6.apiadmin.exception.ConstraintException;
import br.com.grupo6.apiadmin.exception.ResourceNotFoundException;
import br.com.grupo6.apiadmin.model.Administrator;
import br.com.grupo6.apiadmin.service.IAdministratorService;
import br.com.grupo6.apiadmin.validation.PostValidation;
import br.com.grupo6.apiadmin.validation.PutValidation;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin("*")
@Log4j2
public class AdministratorController {

    @Autowired
    private IAdministratorService service;

//    @Autowired
//    private PasswordEncoder encoder;

    @PostMapping("/admin/signup")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Cadastrar pessoa administradora", description = "Este endpoint cadastra uma nova pessoa administradora na base de dados.")
    public Administrator registerNewAdministrator(@Validated(PostValidation.class) @RequestBody Administrator novo, BindingResult br) throws Exception, ConstraintException{

        if (br.hasErrors()){
            throw new ConstraintException(br.getAllErrors().get(0).getDefaultMessage());
        }

//        novo.setPassword(encoder.encode(novo.getPassword()));

        Administrator res = service.createAdministrator(novo);

        return res;
    }

    @PutMapping("/admin/{id}")
    @Operation(summary = "Atualizar um administrador", description = "Este endpoint atualiza um administrador pelo identificador")
    public ResponseEntity<?> changeData(@Validated(PutValidation.class) @RequestBody Administrator data, @PathVariable UUID id, BindingResult br) throws Exception, ConstraintException {
        data.setId(id);
        if (br.hasErrors()){
            throw new ConstraintException(br.getAllErrors().get(0).getDefaultMessage());
        }

        Administrator res = service.changeData(data);

        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/admins")
    public ResponseEntity<List<AdministratorInfosGetDTO>> getAllAdministrators() {
        return ResponseEntity.ok(service.getAllAdministrators());
    }

    @GetMapping("/admins/{id}")
    public ResponseEntity<?> getAdministratorById(@PathVariable UUID id) throws ResourceNotFoundException {
        try {
            AdministratorInfosGetDTO res = service.getAdministratorById(id);
            if (res != null) {
                return ResponseEntity.ok(res);
            }
            return ResponseEntity.notFound().build();
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/admin/auth")
    @Operation(summary = "Autorizar acessos", description = "Este endpoint autoriza acesso a outras aplicações validando o token")
    public ResponseEntity<?> auth(){
        log.info(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return ResponseEntity.ok().body(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

}