package br.com.grupo6.apiadmin.util;

import br.com.grupo6.apiadmin.model.Administrator;

import java.util.UUID;

public class AdministratorCreator {

    public static Administrator createAdministratorToBeSaved(){
        return Administrator.builder()
                .username("testadmin")
                .password("123")
                .email("teste@teste.com")
                .name("admin")
                .build();
    }

    public static Administrator createValidAdministrator(){
        return Administrator.builder()
                .id(UUID.fromString("8b9eccb6-2460-4fe1-8faf-55c4f083b33a"))
                .username("testadmin")
                .password("123")
                .email("teste@teste.com")
                .name("admin")
                .build();
    }

    public static Administrator createValidUpdateAdministrator(){
        return Administrator.builder()
                .id(UUID.fromString("8b9eccb6-2460-4fe1-8faf-55c4f083b33a"))
                .username("testadmin2")
                .password("123")
                .email("teste2@teste.com")
                .name("admin2")
                .build();
    }

}
