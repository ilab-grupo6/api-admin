package br.com.grupo6.apiadmin.service;

//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@DisplayName("Tests for Administrator service")
public class AdministratorServiceTest {

//    @Test
//    @DisplayName("Save persists administrator when Successful")
//    void save_PersistAdministrator_WhenSuccessful(){
//        Administrator administratorToBeSaved = createAdministrator();
//
//        Administrator administratorSaved = this.administratorDAO.save(administratorToBeSaved);
//
//        Assertions.assertThat(administratorSaved).isNotNull();
//        Assertions.assertThat(administratorSaved.getId()).isNotNull();
//        Assertions.assertThat(administratorSaved.getUsername()).isEqualTo(administratorToBeSaved.getUsername());
//        Assertions.assertThat(administratorSaved.getPassword()).isEqualTo(administratorToBeSaved.getPassword());
//        Assertions.assertThat(administratorSaved.getEmail()).isEqualTo(administratorToBeSaved.getEmail());
//        Assertions.assertThat(administratorSaved.getName()).isEqualTo(administratorToBeSaved.getName());
//    }

    @Autowired
    private IAdministratorService service;

//    @Test // eu testo só se o serviço existe
//    @DisplayName("")
//    public void get_existAdministratorService_WhenSuccessful() {
////        Assertions.assertTrue(service.recuperarTodos() == null || service.recuperarTodos() != null);
//        Assertions.assertThat(service.getAllAdministrators< AdministratorInfosGet > == null || service.getAllAdministrators<AdministratorInfosGet>);
//    }

//    @Test
//    public void shouldReturnListaDeUsuario() {
//        Assertions.assertTrue(service.recuperarTodos().size() >= 0);
//    }
//
//    @Test
//    public void shouldExistServicoDetalhes() {
//        Assertions.assertTrue(service.recuperarDetalhes(1) == null || service.recuperarDetalhes(1) != null);
//    }

//    @Test
//    public void shouldExistServicoDetalhes() throws Exception {
//        Assertions.assertTrue(service.getAdministratorById(1) == null || service.getAdministratorById(1) != null);
//    }
//
//    @Test
//    public void shouldReturnUsuarioExistente() {
//        Assertions.assertInstanceOf(Usuario.class, service.recuperarDetalhes(1));
//    }
//
//    @Test
//    public void shouldReturnUsuarioInexistente() {
//        Assertions.assertEquals(null, service.recuperarDetalhes(1000));
//    }
//
//    @Test
//    public void shouldThrowExceptionDeIdInvalido() {
//        Assertions.assertThrows(java.lang.RuntimeException.class, () -> {
//            service.recuperarDetalhes(-1);
//        });
//    }

}
