package br.com.grupo6.apiadmin.dao;

import br.com.grupo6.apiadmin.dto.AdministratorLoginDTO;
import br.com.grupo6.apiadmin.model.Administrator;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static br.com.grupo6.apiadmin.util.AdministratorCreator.createAdministratorToBeSaved;

@DataJpaTest
@DisplayName("Tests for Administrator DAO")
@Log4j2
class AdministratorDAOTest {

    @Autowired
    private AdministratorDAO administratorDAO;

    @Test
    @DisplayName("Save persists administrator when Successful")
    void save_PersistAdministrator_WhenSuccessful(){
        Administrator administratorToBeSaved = createAdministratorToBeSaved();

        Administrator administratorSaved = this.administratorDAO.save(administratorToBeSaved);

        Assertions.assertThat(administratorSaved).isNotNull();
        Assertions.assertThat(administratorSaved.getId()).isNotNull();
        Assertions.assertThat(administratorSaved.getUsername()).isEqualTo(administratorToBeSaved.getUsername());
        Assertions.assertThat(administratorSaved.getPassword()).isEqualTo(administratorToBeSaved.getPassword());
        Assertions.assertThat(administratorSaved.getEmail()).isEqualTo(administratorToBeSaved.getEmail());
        Assertions.assertThat(administratorSaved.getName()).isEqualTo(administratorToBeSaved.getName());
    }

    @Test
    @DisplayName("Save updates administrator when Successful")
    void save_UpdatesAdministrator_WhenSuccessful(){
        Administrator administratorToBeSaved = createAdministratorToBeSaved();

        Administrator administratorSaved = this.administratorDAO.save(administratorToBeSaved);

        administratorSaved.setName("teste2");
        administratorSaved.setPassword("456");
        administratorSaved.setEmail("teste2@teste.com");
        administratorSaved.setName("admin2");

        Administrator administratorUpdated = this.administratorDAO.save(administratorSaved);

        Assertions.assertThat(administratorUpdated).isNotNull();
        Assertions.assertThat(administratorUpdated.getId()).isNotNull();
        Assertions.assertThat(administratorUpdated.getUsername()).isEqualTo(administratorSaved.getUsername());
        Assertions.assertThat(administratorUpdated.getPassword()).isEqualTo(administratorSaved.getPassword());
        Assertions.assertThat(administratorUpdated.getEmail()).isEqualTo(administratorSaved.getEmail());
        Assertions.assertThat(administratorUpdated.getName()).isEqualTo(administratorSaved.getName());
    }

    @Test
    @DisplayName("Find By Username and Email for login an administrator when Successful")
    void findByUsernameOrEmail_ReturnsAnAdministrator_WhenSuccessful(){
        Administrator administratorToBeSaved = createAdministratorToBeSaved();

        Administrator administratorSaved = this.administratorDAO.save(administratorToBeSaved);

        Administrator administratorLoginUsernameOrEmail = this.administratorDAO.findByUsernameOrEmail(administratorSaved.getUsername(), administratorSaved.getEmail());

        Assertions.assertThat(administratorLoginUsernameOrEmail).toString();
        Assertions.assertThat(administratorLoginUsernameOrEmail).isNotNull();

    }

    @Test
    @DisplayName("Find By Username and Email returns null when an administrator is found")
    void findByUsernameOrEmail_ReturnsNull_WhenAdministratorIsNotFound(){

        Administrator administrator = this.administratorDAO.findByUsernameOrEmail("UserNameNotFound", "EmailNotFound");

       Assertions.assertThat(administrator).isNull();

    }


//    private Administrator createAdministrator(){
//        return Administrator.builder()
//                .username("testadmin")
//                .password("123")
//                .email("teste@teste.com")
//                .name("admin")
//                .build();
//    }

}