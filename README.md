# API ADMIN [EM CONSTRUÇÃO]

## URL: 

### Endpoints:

#### - POST: /login

Este endpoint faz a autenticação da pessoa administradora já cadastrada na base de dados e retorna um token de autenticação para acesso aos endpoints protegidos.

##### Requisição:

```json
{
    "email": "pessoa@email.com",
    "password": "senhaDaPessoa"
}
```
##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
    "token": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJHZXJhbGRvIiwiaXNzIjoiUHJvZklzaWRyb0lmb29kIiwiZXhwIjoxNjQ3NDY2NTMxfQ.yFfygoULOM3vkYU0ZlwhKeNenTPU-wPHyQPZepE8wao"
}
```

###### Em caso de email e/ou senha incorreta(s) (código `401`):

```json
{
    "message": "E-mail e/ou senha incorreto(s)"
}
```

###### Em caso de ausência do email e/ou senha (código `400`):

```json
{
    "message": "Favor, informar e-mail e senha"
}
```

#### - POST: /admin/signup

Este endpoint cadastra uma nova pessoa administradora na base de dados.

##### Requisição:

```json
{
    "username": "usernameAdministrador",
    "email": "pessoa@email.com",
    "name": "pessoa administradora",
    "password": "senhaDaPessoa",
}
```

Validações:

* Validação de e-mail
* Verifica se já possui cadastro com o mesmo e-mail no banco de dados
* Todos os campos são obrigatórios

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `201`):

```json
{
    "id": 1,
    "email": "pessoa@email.com",
    "name": "pessoa administradora",
    "username": "usernameAdministrador",
    "password": "senhaDaPessoaCriptografada"
}
```

###### Em caso de algum erro na requisição (como e-mail já existente ou problema com a senha) (código `40X`):

```json
{
    "message": "mensagem personalizada aqui"
}
```

#### - GET: /admin/auth

##### Resposta:

###### Em caso de sucesso, será retornado somente o código `200`.

###### Caso contrário, será retornado somente o código `401`.
