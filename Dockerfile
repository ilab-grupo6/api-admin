FROM openjdk:17.0.2-jdk
COPY ./newrelic/newrelic.jar .
COPY ./newrelic/newrelic.yml .
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENV NEW_RELIC_APP_NAME="Admin API"
ENV NEW_RELIC_LOG_FILE_NAME="STDOUT"
EXPOSE 8080
ENTRYPOINT ["java", "-javaagent:./newrelic.jar", "-jar","/app.jar"]
